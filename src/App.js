import React, { Component } from 'react';
import { Switch, Route, Redirect, withRouter } from 'react-router-dom';
import Layout from './hoc/Layout/Layout';
import Home from './containers/Home/Home';
import Login from './containers/Auth/Login';
import Logout from './containers/Auth/Logout';
import Register from './containers/Auth/Register';
import { connect } from 'react-redux';
import { checkAuthState } from './store/actions/authActions';
import { getAllCards } from './store/actions/cardActions';
import HomeUnAuth from './containers/Home/HomeUnAuth';

class App extends Component {

  componentDidMount() {
    // Keep auth state
    this.props.onKeepSigninState();
    this.props.getAllCards();
  }

  render() {
    let routes = (
      <Switch>
        <Route path="/login" component={Login} />
        <Route path="/register" component={Register} />
        <Route path="/" exact component={HomeUnAuth} />
        <Redirect to="/" />
      </Switch>
    );

    if (this.props.isAuth) {
      routes = (
        <Switch>
          <Route path="/logout" component={Logout} />
          <Route path="/" exact component={Home} />
          <Redirect to="/" />
        </Switch>
      );
    }

    return (
      <div>
        <Layout>
          {routes}
        </Layout>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    isAuth: state.auth.token != null,
  };
};

const mapDispatchToProps = {
  onKeepSigninState: checkAuthState,
  getAllCards,
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
