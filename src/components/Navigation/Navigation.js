import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux';
// import Aux from '../../hoc/Auxiliary/Auxiliary';

class Navigation extends Component {

    removeCartItem = (item) => {
      // this.props.removeCartItem(item.product._id, item.size);
    }

    render() {
        return (
          <div>
            <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
              <a className="navbar-brand" href="/">Cards game marketplace</a>
              {!this.props.isAuth ? (
                <NavLink to="/login" className="ml-auto">
                  <button className="btn btn-success">Login</button>
                </NavLink>
              ) : (
                <div className="text-light ml-auto"> 
                  Hello {this.props.email} | {"  "}
                  <NavLink className="logout text-light" to="/logout">
                    <i className="fa fa-sign-out"></i>Logout
                  </NavLink>
                </div>
              )}
            </nav>
          </div>
        );
    }
}

const mapStateToProps = state => {
    return {
      isAuth: state.auth.token != null,
      userId: state.auth.userId,
      email: state.auth.email
    };
};

const mapDispatchToProps = {
    // removeCartItem: removeCartItem
};

export default connect(mapStateToProps, mapDispatchToProps)(Navigation);