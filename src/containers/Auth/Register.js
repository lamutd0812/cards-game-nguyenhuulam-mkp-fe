import React, { Component } from 'react';
import { Redirect, Link } from 'react-router-dom';
import { register, resetRegisterState } from '../../store/actions/authActions';
import { connect } from 'react-redux';
class Register extends Component {
  
  state = {
    email: '',
    password: '',
  }

  componentDidMount() {
    this.props.onResetRegisterState();
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    // if (nextProps.error) {
    //   this.props.onResetRegisterState();
    // }
    if (nextProps.isSignedUp && !nextProps.error) {
      this.props.history.push('/login');
    }
  }

  inputChangeHandler = (event) => {
    const controlName = event.target.name;
    switch (controlName) {
      case 'email':
        this.setState({
          email: event.target.value,
        });
        break;
      case 'password':
        this.setState({
          password: event.target.value,
        });
        break;
      default:
        break;
    }
  };

  formSubmitHandler = (event) => {
    event.preventDefault();
    this.props.onRegister(this.state.email, this.state.password);
  };

  render() {
    let authRedirect = null;
    if (this.props.registerSuccess) {
      authRedirect = <Redirect to="/login" />
    }

    return (
      <div className="container">
        {authRedirect}
        <div className="row">
          <div className="col-md-4 offset-md-4">
            <div className="login-form bg-light mt-4 p-4">
              <form onSubmit={this.formSubmitHandler}>
                <h4 className="text-center">Signup page</h4>
                <div className="col-12">
                  <label>Email address</label>
                  <input
                    type="text"
                    name="email"
                    className="form-control"
                    onChange={this.inputChangeHandler} />
                </div>

                <div className="col-12">
                  <label>Password</label>
                  <input
                    type="password"
                    name="password"
                    className="form-control"
                    onChange={this.inputChangeHandler} />
                </div>

                {this.props.error ? (
                  <div className="col-12">
                    <div className="text-danger">{this.props.error}</div>
                  </div>
                ) : null}

                <div className="col-12 mt-2">
                  <button
                    type="submit"
                    className="btn btn-dark float-end"
                  >Signup</button>
                </div>
              </form>
                <hr className="mt-4" />
                <div className="col-12">
                  <p className="text-center mb-0">Already have an account?
                    <Link to="/login"> Signin</Link>
                  </p>
                </div>
            </div>
          </div>
        </div>
      </div>    
    );
  }
}

const mapStateToProps = state => {
  return {
    loading: state.auth.loading,
    error: state.auth.error,
    isSignedUp: state.auth.isSignedUp
  };
};

const mapDispatchToProps = {
  onRegister: register,
  onResetRegisterState: resetRegisterState
};

export default connect(mapStateToProps, mapDispatchToProps)(Register);