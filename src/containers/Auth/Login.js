import React, { Component } from 'react';
import { Redirect, Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { auth } from '../../store/actions/authActions';

class Login extends Component {

  state = {
    email: '',
    password: '',
  }

  inputChangeHandler = (event) => {
    const controlName = event.target.name;
    switch (controlName) {
      case 'email':
        this.setState({
          email: event.target.value,
        });
        break;
      case 'password':
        this.setState({
          password: event.target.value,
        });
        break;
      default:
        break;
    }
  };

  formSubmitHandler = (event) => {
    event.preventDefault();
    this.props.onAuth(this.state.email, this.state.password);
  };

  render() {
    let authRedirect = null;
    if (this.props.isAuth) {
      authRedirect = <Redirect to="/" />
    }

    return (
      <div className="container">
        {authRedirect}
        <div className="row">
          <div className="col-md-4 offset-md-4">
            <div className="login-form bg-light mt-4 p-4">
              <form onSubmit={this.formSubmitHandler}>
                <h4 className="text-center">Login page</h4>
                <div className="col-12">
                  <label>Email address</label>
                  <input
                    type="text"
                    name="email"
                    className="form-control"
                    onChange={this.inputChangeHandler} />
                </div>

                <div className="col-12">
                  <label>Password</label>
                  <input
                    type="password"
                    name="password"
                    className="form-control"
                    onChange={this.inputChangeHandler} />
                </div>
                
                {this.props.error ? (
                  <div className="col-12">
                    <div className="text-danger">{this.props.error}</div>
                  </div>
                ) : null}

                <div className="col-12 mt-2">
                  <button
                    type="submit"
                    className="btn btn-dark float-end"
                  >Login</button>
                </div>
              </form>
                <hr className="mt-4" />
                <div className="col-12">
                  <p className="text-center mb-0">Have not account yet?
                    <Link to="/register"> Signup</Link>
                  </p>
                </div>
            </div>
          </div>
        </div>
      </div>    
    );
  }
}

const mapStateToProps = state => {
    return {
      loading: state.auth.loading,
      error: state.auth.error,
      isAuth: state.auth.token !== null
    };
};

const mapDispatchToProps = {
    onAuth: auth
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);