import React, { Component } from 'react';
import AllCards from '../Cards/AllCards';

class HomeUnAuth extends Component {

  render() {
    return (
      <div>
        <div className="form-group pt-3 pl-3">
          <h6 className="font-weight-bold ml-3"> Login to buy cards and see your cards </h6>
        </div>
        <AllCards />
      </div>
    );
  }
}

export default HomeUnAuth;
