import React, { Component } from 'react';
import MyCards from '../Cards/MyCards';
import { getMyCards } from '../../store/actions/cardActions';
import { connect } from 'react-redux';
import AllCards from '../Cards/AllCards';
import 'react-toastify/dist/ReactToastify.css';
import { ToastContainer, toast } from 'react-toastify';

class Home extends Component {

    componentDidMount() {
      window.scrollTo(0, 0);
      if (this.props.userId && !this.props.myCards.length) {
        this.props.getMyCards();
      }
    }

    render() {
      return (
        <div>
          <MyCards />
          <AllCards />
          <ToastContainer autoClose={2000} />
          {this.props.error ? toast.error('Error: ' + this.props.error) : null}
        </div>
      );
    }
}

const mapStateToProps = (state) => {
  return {
    userId: state.auth.userId,
    email: state.auth.email,
    myCards: state.card.myCards,
    error: state.card.error,
  }
}

const mapDispatchToProps = {
  getMyCards,
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);
