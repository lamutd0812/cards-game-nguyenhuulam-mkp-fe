import React, { Component } from 'react';
import Aux from '../../hoc/Auxiliary/Auxiliary';
import { connect } from 'react-redux';
import 'react-toastify/dist/ReactToastify.css';
import { toast } from 'react-toastify';
import ConfirmDialog from '../../components/UI/ConfirmDialog/ConfirmDialog2';
import { getMyCards, buyCards, resetBuyCardsSuccess } from '../../store/actions/cardActions';

class AllCards extends Component {

  state = {
    cardIdToBuy: '',
  }

  setCardIdToBuy = (cardId) => {
    this.setState({
      cardIdToBuy: cardId,
    });
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.buyCardsSuccess && !nextProps.error) {
      toast.success("Buy card sussessfully!");
      this.props.resetBuyCardsSuccess();
      this.props.getMyCards();
    }
    if (nextProps.error) {
      this.props.resetBuyCardsSuccess();
    }
  }

  confirmSubmitHandler = () => {
    const cardId = this.state.cardIdToBuy;
    const quantity = 1;
    this.props.buyCards(cardId, quantity);
  }

  render() {
    const contentWrapper = (
      <div className="form-group pt-3 pl-3">
        <h6 className="font-weight-bold ml-3"> Cards Shop </h6>
        <div className="form-group ml-3">
          <div className="row">
          <Aux>
            {this.props.allCards.length > 0 ? (
              this.props.allCards.map(card => (
                <Aux key={card._id}>
                  {/* my card item */}
                  <div className="card" style={{ width: '18rem' }}>
                    <div className="card-body">
                      <h5 className="card-title">Card name: {card.name}</h5>
                      <p className="card-text"> Power: {card.power}</p>
                      <p className="card-text"> Price: {card.price}</p>
                      {this.props.userId ? (
                        <button
                          className="btn btn-primary"
                          data-toggle="modal"
                          data-target="#dialogTest"
                          onClick={() => this.setCardIdToBuy(card._id)}
                        >
                          Buy card
                        </button>
                      ): null}
                    </div>
                  </div>
                </Aux>
              ))
            ): (
              <div>Shop have no cards</div>
            )}
          </Aux>
          </div>
        </div>
      </div>
    );

    return (
      <Aux>
        {contentWrapper}
        <ConfirmDialog
          title="Buy card confirmation"
          message="Buy this card?"
          confirm={this.confirmSubmitHandler} />
      </Aux>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    userId: state.auth.userId,
    email: state.auth.email,
    allCards: state.card.allCards,
    buyCardsSuccess: state.card.buyCardsSuccess,
    error: state.card.error,
  }
}

const mapDispatchToProps = {
  getMyCards,
  buyCards,
  resetBuyCardsSuccess,
}

export default connect(mapStateToProps, mapDispatchToProps)(AllCards);