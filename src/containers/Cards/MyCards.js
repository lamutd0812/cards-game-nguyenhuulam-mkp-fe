import React, { Component } from 'react';
import Aux from '../../hoc/Auxiliary/Auxiliary';
import { connect } from 'react-redux';
import ConfirmDialog from '../../components/UI/ConfirmDialog/ConfirmDialog';
import 'react-toastify/dist/ReactToastify.css';
import { toast } from 'react-toastify';
import { getMyCards, returnCards, resetReturnCardsSuccess } from '../../store/actions/cardActions';

class MyCards extends Component {

  state = {
    cardIdToSell: '',
  }

  setCardIdToSell = (cardId) => {
    this.setState({
      cardIdToSell: cardId,
    });
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.returnCardsSuccess && !nextProps.error) {
      toast.success("Return card sussessfully!");
      this.props.resetReturnCardsSuccess();
      this.props.getMyCards();
    }
  }

  confirmSubmitHandler = () => {
    const cardId = this.state.cardIdToSell;
    const quantity = 1;
    this.props.returnCards(cardId, quantity);
  }

  render() {
    const contentWrapper = (
      <div className="form-group pt-3 pl-3">
        <h6 className="font-weight-bold ml-3"> My Cards</h6>
        <div className="form-group ml-3">
          <div className="row">
            {this.props.userId ? (
              <Aux>
                {this.props.myCards.length > 0 ? (
                  this.props.myCards.map(card => (
                    <Aux key={card._id}>
                      {/* my card item */}
                      <div className="card" style={{ width: '18rem' }}>
                        <div className="card-body">
                          <h5 className="card-title">Card name: {card.cardData.name}</h5>
                          <p className="card-text"> Power: {card.cardData.power}</p>
                          <p className="card-text"> Price: {card.cardData.price}</p>
                          <p className="card-text"> Quantity: {card.quantity}</p>
                          <button
                            className="btn btn-dark"
                            data-toggle="modal"
                            data-target="#confirmDialogModal"
                            onClick={() => this.setCardIdToSell(card.cardId)}
                          >
                            Return card
                          </button>
                        </div>
                      </div>
                    </Aux>
                  ))
                ): (
                  <div>You have no cards</div>
                )}
              </Aux>
            ): (
              <div>Login to see your cards</div>
            )}
          </div>
        </div>
      </div>
    );
    return (
      <Aux>
        {contentWrapper}
        <ConfirmDialog
          title="Return card confirmation"
          message="Rerturn this card and get coins back?"
          confirm={this.confirmSubmitHandler} />
      </Aux>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    userId: state.auth.userId,
    email: state.auth.email,
    myCards: state.card.myCards,
    returnCardsSuccess: state.card.returnCardsSuccess,
    error: state.card.error,
  }
}

const mapDispatchToProps = {
  getMyCards,
  returnCards,
  resetReturnCardsSuccess,
}

export default connect(mapStateToProps, mapDispatchToProps)(MyCards);