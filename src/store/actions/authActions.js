import axios from '../../util/axios';
import * as actionTypes from './actionTypes';

const authStart = () => {
  return {
    type: actionTypes.AUTH_START
  };
};

const authSuccess = (token, userId, email) => {
  return {
    type: actionTypes.AUTH_SUCCESS,
    token: token,
    userId: userId,
    email: email,
  };
};

const registerSuccess = (message) => {
  return {
    type: actionTypes.REGISTER_SUCCESS,
    isSignedUp: true
  }
}

const authFailed = (error) => {
  return {
    type: actionTypes.AUTH_FAILED,
    error: error
  };
};

export const logout = () => {
  // clear local storage
  localStorage.removeItem('token');
  localStorage.removeItem('userId');
  localStorage.removeItem('expirationDate');
  localStorage.removeItem('email');
  return {
      type: actionTypes.AUTH_LOGOUT
  };
};

const checkAuthTimeout = (expirationTime) => (dispatch) => {
  setTimeout(() => {
    dispatch(logout);
  }, expirationTime * 1000);
};

export const auth = (email, password) => (dispatch) => {
  dispatch(authStart());
  const authData = {
    email: email,
    password: password
  };
  axios.post('/auth/signin', authData)
    .then(res => {
      const resp = res.data;
      const expirationDate = new Date(new Date().getTime() + 86400); // 1h
      localStorage.setItem('token', resp.data.accessToken);
      localStorage.setItem('userId', resp.data.userId);
      localStorage.setItem('email', resp.data.email);
      localStorage.setItem('expirationDate', expirationDate);
      dispatch(authSuccess(resp.data.accessToken, resp.data.userId, resp.data.email));
      dispatch(checkAuthTimeout(60 * 60));
    })
    .catch(error => {
      dispatch(authFailed(error.response.data.message));
    });
};

export const register = (email, password, fullname) => (dispatch) => {
  dispatch(authStart());
  const registerData = {
    email: email,
    password: password,
  };
  axios.post('/auth/signup', registerData)
    .then(res => {
      dispatch(registerSuccess(res.data.data.message));
    })
    .catch(error => {
      dispatch(authFailed(error.response.data.message));
    });
};

export const resetRegisterState = () => (dispatch) => {
  dispatch({
    type: actionTypes.RESET_REGISTER_STATE
  });
};

//keep auth state
export const checkAuthState = () => (dispatch) => {
  const token = localStorage.getItem('token');
  if (!token) {
    dispatch(logout());
  } else {
    const expirationDate = new Date(Date.parse(localStorage.getItem('expirationDate')));
    if (expirationDate <= new Date()) {
      dispatch(logout());
    } else {
      const userId = localStorage.getItem('userId');
      const email = localStorage.getItem('email');
      dispatch(authSuccess(token, userId, email));
      dispatch(checkAuthTimeout((expirationDate.getTime() - new Date().getTime()) / 1000));
    }
  }
};