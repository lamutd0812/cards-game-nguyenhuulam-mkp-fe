import axios from '../../util/axios';
import * as actionTypes from './actionTypes';

const cardStart = () => {
  return {
    type: actionTypes.CARDS_START
  };
};

const cardErrors = (error) => {
  return {
    type: actionTypes.CARDS_ERROR,
    error: error
  }
};

const getMyCardsSuccess = (data) => {
  return {
    type: actionTypes.GET_MY_CARDS_SUCCESS,
    myCards: data,
  }
};

const getAllCardsSuccess = (data) => {
  return {
    type: actionTypes.GET_ALL_CARDS_SUCCESS,
    allCards: data,
  }
};

const buyCardsSuccess = () => {
  return {
    type: actionTypes.BUY_CARDS_SUCCESS,
    buyCardsSuccess: true,
  }
}

export const resetBuyCardsSuccess = () => (dispatch) => {
  dispatch({
    type: actionTypes.RESET_BUY_CARDS_SUCCESS
  })
};

const returnCardsSuccess = () => {
  return {
    type: actionTypes.RETURN_CARDS_SUCCESS,
    returnCardsSuccess: true,
  }
}


export const resetReturnCardsSuccess = () => (dispatch) => {
  dispatch({
    type: actionTypes.RESET_RETURN_CARDS_SUCCESS
  })
};

export const getMyCards = () => (dispatch, getState) => {
  const token = getState().auth.token;
  axios.get('/cards/my-cards', {
    headers: { 'Authorization': `Bearer ${token}` }
  })
  .then(res => {
    dispatch(getMyCardsSuccess(res.data.data));
  })
  .catch(err => {
    dispatch(cardErrors(err.message));
  });
}

export const getAllCards = () => (dispatch) => {
  axios.get('/cards')
  .then(res => {
    dispatch(getAllCardsSuccess(res.data.data));
  })
  .catch(err => {
    dispatch(cardErrors(err.message));
  });
}

export const buyCards = (cardId, quantity) => (dispatch, getState) => {
  dispatch(cardStart());
  const token = getState().auth.token;
  const body = { cardId, quantity };
  axios.post('/cards/buy-cards', body, {
    headers: { 'Authorization': `Bearer ${token}` }
  })
  .then(res => {
    if (res.data.statusCode === 200 && res.data.success) {
      dispatch(buyCardsSuccess());
    }
  })
  .catch(err => {
    dispatch(cardErrors(err.response.data.message));
  });
}

export const returnCards = (cardId, quantity) => (dispatch, getState) => {
  dispatch(cardStart());
  const token = getState().auth.token;
  const body = { cardId, quantity };
  axios.put('/cards/return-cards', body, {
    headers: { 'Authorization': `Bearer ${token}` }
  })
  .then(res => {
    if (res.data.statusCode === 200 && res.data.success) {
      dispatch(returnCardsSuccess());
    }
  })
  .catch(err => {
    dispatch(cardErrors(err.response.data.message));
  });
}