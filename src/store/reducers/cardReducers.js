import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../../util/utility';

const initialState = {
  allCards: [],
  myCards: [],
  message: '',
  error: null,
  buyCardsSuccess: false,
  returnCardsSuccess: false,
};

const cardStart = (state) => {
  return updateObject(state, { error: null });
};

const fetchCardsError = (state, action) => {
  return updateObject(state, {
    error: action.error,
  });
};

const getMyCardsSuccess = (state, action) => {
  return updateObject(state, {
    myCards: action.myCards,
    error: null
  });
};

const getAllCardsSuccess = (state, action) => {
  return updateObject(state, {
    allCards: action.allCards,
    error: null
  });
};

const buyCardsSuccess = (state, action) => {
  return updateObject(state, {
    buyCardsSuccess: action.buyCardsSuccess,
    error: null
  });
};

const returnCardsSuccess = (state, action) => {
  return updateObject(state, {
    returnCardsSuccess: action.returnCardsSuccess,
    error: null
  });
};

const resetBuyCardsSuccess = (state) => {
  return updateObject(state, {
    buyCardsSuccess: false,
    error: null
  });
};

const resetReturnCardsSuccess = (state) => {
  return updateObject(state, {
    returnCardsSuccess: false,
    error: null
  });
};

const cardReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.CARDS_START: return cardStart(state);
    case actionTypes.CARDS_ERROR: return fetchCardsError(state, action);

    case actionTypes.GET_MY_CARDS_SUCCESS: return getMyCardsSuccess(state, action);
    case actionTypes.GET_ALL_CARDS_SUCCESS: return getAllCardsSuccess(state, action);

    case actionTypes.BUY_CARDS_SUCCESS: return buyCardsSuccess(state, action);
    case actionTypes.RETURN_CARDS_SUCCESS: return returnCardsSuccess(state, action);

    case actionTypes.RESET_BUY_CARDS_SUCCESS: return resetBuyCardsSuccess(state);
    case actionTypes.RESET_RETURN_CARDS_SUCCESS: return resetReturnCardsSuccess(state);

    default: return state;
  }
}

export default cardReducer;